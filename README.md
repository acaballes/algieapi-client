# README #

# Desciption #
* This is a simple application client to communicate with the REST api service

# Pre-requisite#
* You should be able to have a cUrl php extention tool installed in your machine, if not you can run it by typing:
       - sudo apt-get install php5-curl
       - sudo service apache2 restart

# Installation # 
       - create a directory (mkdir create/directory/path)
       - cd create/directory/path
       - git clone https://acaballes@bitbucket.org/acaballes/algieapi-client.git
       - you can either create a virtual host for this client in the apache2/nginx configuration or just the default config will do
       - visit config/config.php and you may change the  API_HOST url (the AUTHENTICATION_KEY is hardcoded if you changed it, you may not allowed to get the data from the api or you will also mannually change both (client and the api authentication key))
       - now you're ready to go to access the algieapi REST service

# Features #
       - View lists of products
       - Add new product
       - Delete product
       - Edit product
       
# Tools used #
        - Jquery
        - Bootstrap
        - Datatable plugin