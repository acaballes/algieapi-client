<?php
/**
 * @author Algie Caballes <algie.developer@gmail.com>
 * @desc A class tat handles api requests
 */
namespace Api;

class ApiCalls
{
    public function getRequest()
    {
	$curl = curl_init();
    	    curl_setopt_array($curl, array(
    	    CURLOPT_RETURNTRANSFER => 1,
    	    CURLOPT_URL => API_HOST.'/index.php?api_name=product',
	    CURLOPT_HTTPHEADER => array('Authenticaton-Header : '.AUTHENTICATION_KEY)
    	));
    	$result = curl_exec($curl);
    	curl_close($curl);
	return $result;
    }

    public function getByIdRequest($id)
    {
	$id = "id={$id}";
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => API_HOST.'/index.php?api_name=product&'.$id,
	    CURLOPT_HTTPHEADER => array('Authenticaton-Header : '.AUTHENTICATION_KEY)
        ));
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function deleteRequest($id)
    {
        $id = "id={$id}";
        $curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authenticaton-Header : '.AUTHENTICATION_KEY));
	curl_setopt($curl, CURLOPT_URL,API_HOST.'/index.php?api_name=product&'.$id);
    	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    	$result = curl_exec($curl);
    	$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $result;
    }

    public function postRequest($data)
    {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authenticaton-Header : ' .AUTHENTICATION_KEY));
        curl_setopt($curl, CURLOPT_URL,API_HOST.'/index.php?api_name=product');
	curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	$result = curl_exec($curl);
	curl_close($curl);
	return $result;
    }

    public function putRequest($data)
    {
	$params = http_build_query($data);
        $curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authenticaton-Header : '.AUTHENTICATION_KEY));
        curl_setopt($curl, CURLOPT_URL,API_HOST.'/index.php?api_name=product&'.$params);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        #curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	#curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
        $result = curl_exec($curl);
	$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $result;
    }
}
?>
