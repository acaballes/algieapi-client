<?php
/**
 * @author Algie Caballes <algie.developer@gmail.com>
 * @desc A class that handle api request methods
 */
namespace Actions;
require_once('ApiCalls.php');

class Actions
{
    public function getMethod()
    {
	$api = new \Api\ApiCalls();
	$result = $api->getRequest();
	$data = json_decode($result, true);
	return isset($data['items']) ? $data['items'] : array();
    }

    public function getByIdMethod()
    {
	$api = new \Api\ApiCalls();
        $result = $api->getByIdRequest();
        $data = json_decode($result, true);
        return $data;
    }

    public function deleteMethod($id)
    {
	$api = new \Api\ApiCalls();
        $result = $api->deleteRequest($id);
        return $result;
    }

    public function postMethod($data)
    {
	$api = new \Api\ApiCalls();
        $result = $api->postRequest($data);
        return $result;
    }
 
    public function putMethod($data)
    {
        $api = new \Api\ApiCalls();
        $result = $api->putRequest($data);
        return $result;
    }
}

?>
