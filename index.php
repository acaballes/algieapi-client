<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Api Client</title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datatables.css" rel="stylesheet">
  </head>
  <body>
    <?php require_once('config/config.php'); ?>
    <?php require_once('src/Actions.php'); ?>
    <div class="" style="padding:50px;">
	<div><h1>Products List</h1></div>
	<div><button class="btn btn-success" onclick="addModal()">Add product</button></div><br>
	<?php
	    $data = new Actions\Actions();
	    if (isset($_GET['action'])) {
		if ($_GET['action'] == 'delete') {
		   $data->deleteMethod($_GET['id']); 
		   header('location: index.php');
		} else if ($_GET['action'] == 'add') {
		    $data->postMethod($_POST);
		    header('location: index.php');
		} else if ($_GET['action'] == 'edit') {
                    $data->putMethod($_POST);
                    header('location: index.php');
                }
	    }
	    $results = $data->getMethod();
	    $table = "<table id='products' class='display' cellspacing='0' width='100%'>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th>Product Name</th>";
            $table .= "<th>Description</th>";
            $table .= "<th>Price</th>";
	    $table .= "<th>Actions</th>";
            $table .= "</thead>";
	    $table .= "<tbody>";
	    foreach ($results as $item) {
		$table .= "<tr id='".$item['id']."'><td>{$item['name']}</td>";
		$table .= "<td>{$item['description']}</td>";
		$table .= "<td>{$item['price']}</td>";
		$table .= "<td><div class='dropdown'>";
		$table .= "<button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>Select Action<span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='dropdownMenu1'><li><a href='javascript:deleteModal(".$item['id'].");'>Delete</a></li><li><a href='javascript:editModal(".$item['id'].")'>Edit</a></li></ul></div></td>";
		$table .= "</tr>";
	    }
	    $table .= "</tbody>";
	    $table .= "</table>";
	    echo $table;
	?>
    </div>
    <div class="modal fade" id="deleteRecordModal" tabindex="-1" role="dialog" aria-labelledby="deleteRecordModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
		<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warning!</h4>
                </div>
                <div class="modal-body">
		    <p>Are you sure sure you want to delete this product?</p>
                </div>
                <div class="modal-footer">
		    <button type="button" class="btn btn-danger" onclick="confirmDelete()">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addRecordModal" tabindex="-1" role="dialog" aria-labelledby="addRecordModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Product edit form</h4>
                </div>
                <div class="modal-body">
		    <form action="index.php?action=add" method="post">
                    <div class="form-group">
                        <label for="name">Product name:</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="text" name="price" class="form-control" id="price">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editRecordModal" tabindex="-1" role="dialog" aria-labelledby="editRecordModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Product form</h4>
                </div>
                <div class="modal-body">
                    <form action="index.php?action=edit" method="post">
                    <div class="form-group">
                        <label for="name">Product name:</label>
                        <input type="text" name="name" class="form-control" id="name-field">
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" class="form-control" id="description-field"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="text" name="price" class="form-control" id="price-field">
                    </div>
		    <input type="hidden" name="id" id='product-id-field'>
                    <button type="submit" class="btn btn-default">Save</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/datatables.js"></script>
    <script type="text/javascript">
	$(document).ready(function() {
    	    $('#products').DataTable();
	} );
	function deleteModal(id){
	    $('#deleteRecordModal').attr('product-id', id);
	    $('#deleteRecordModal').modal('show');
	}
	function confirmDelete() {
	    location.href = "index.php?action=delete&id="+$('#deleteRecordModal').attr('product-id');
 	}
	function addModal() {
	    $('#addRecordModal').modal('show');
	}
	function editModal(id) {
	    var cols = $('#'+id+' td');
	    $('#product-id-field').val(id);
	    $('#name-field').val(cols[0].innerHTML);
	    $('#description-field').val(cols[1].innerHTML);
	    $('#price-field').val(cols[2].innerHTML);
	    $('#editRecordModal').modal('show');
	}
    </script>
  </body>
</html>
